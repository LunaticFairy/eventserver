package EventServer::Plugin::Test;

use Moose;

with 'EventServer::Role::Plugin';

sub BUILD {
	my $self = shift;

	$self->name("Test plugin");
	$self->version("1.0");
	$self->priority(1);
}

sub initialize {
	my $self = shift;
			
	print "Initialized a plugin :D\n";
}
