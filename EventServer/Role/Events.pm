package EventServer::Role::Events;

use Moose::Role;

has 'events' => (
	is => 'rw',
	isa => 'HashRef',
	default => {{}}
);

1;
