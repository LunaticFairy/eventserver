package EventServer::Role::Plugin;

use Moose::Role;

has 'name' => (
	is => 'rw',
	isa => 'Str'
);

has 'version' => (
	is => 'rw',
	isa => 'Str'
);

has 'priority' => (
	is => 'rw',
	isa => 'Int'
);

sub load {}
sub initialize {}

1;
