package EventServer::Kernel;

use strict;
use Moose;
use POE qw(Component::Server::TCP);
use threads;
use JSON;
use EventServer::Plugin;

has 'address' => (
	is => 'rw',
	isa => 'Str'
);

has 'port' => (
	is => 'rw',
	isa => 'Int'
);

has 'plugin' => (
	is => 'ro',
	isa => 'EventServer::Plugin',
	default => sub {EventServer::Plugin->new;}
);

sub BUILD {
	my $self = shift;
	$self->plugin->load;
	POE::Component::Server::TCP->new
   	(
        	Port => $self->port,
        	ClientConnected => sub {
			my $thread = threads->create(sub {
				$self->ClientConnected(@_);
			}, @_);
			$thread->detach;
		},
		ClientDisconnected => sub {
			my $thread = threads->create(sub {
				$self->ClientDisconnected(@_);
			}, @_);
			$thread->detach;
		},
        	ClientInput => sub {
			my $thread = threads->create(sub {
				$self->ClientInput(@_);
			}, @_);
			$thread->detach;
		}
    	);
	POE::Kernel->run;
}

sub ClientConnected {
	my $self = shift;
	print "Client connected: [$_[HEAP]{remote_ip}:$_[HEAP]{remote_port}]\n";
}

sub ClientDisconnected {
	my $self = shift;
	print "Client disconnected: [$_[HEAP]{remote_ip}:$_[HEAP]{remote_port}]\n";
}

sub ClientInput {
	my $self = shift;
	print "Input received\n";
	sleep(5);
	print "Sleep ended\n";
}

1;
