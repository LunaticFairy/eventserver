package EventServer::Plugin;

use strict;
use Moose;
use Moose::Autobox;
use Plugin::Tiny;
use Carp;
use Cwd;

has 'pluginsystem' => (
	is => 'ro',
	isa => 'Plugin::Tiny',
	default => sub {Plugin::Tiny->new(prefix=>'EventServer::Plugin::')}
);

has 'plugins' => (
	is => 'rw',
	isa => 'ArrayRef',
	default => sub {[]}
);

has 'storage' => (
	is => 'rw',
	isa => 'HashRef',
	default => sub {{}}
);

sub getdir { #Subroutine that gets every perl module file in the plugin directory.
	opendir my $dir, getcwd()."/EventServer/Plugin/" or die "Cannot open directory: $!";
	my @files = grep {s/\.pm$//} readdir $dir;
	closedir $dir;
	return @files;
}

sub load {
	my $self = shift;
	my @plugins;
	
	#Load each plugin into the plugins array before we initialize them
	foreach my $file (getdir) {
		my $plugin_name = ($file =~ s/\.pm//r);
		my $plugin = $self->pluginsystem->register(plugin=>$plugin_name);
		
		if($plugin->does('EventServer::Role::Plugin')) {
			print "Loaded plugin [", $plugin->name, "], version [", $plugin->version, "], priority [", $plugin->priority, "]\n";
			push @plugins, $plugin;
		} else { 
			carp("Plugin [EventServer::Plugin::$plugin_name] doesnt implement role [EventServer::Role::Plugin]");
		}
	}
	
	#Initialize plugins
	eval{$_->initialize} foreach (sort {$a->priority <=> $b->priority} @plugins);
	
	#Check if initialization failed
	if($@) {
		carp("Initialization of plugin failed: $@");
		return 1;
	}
	
	#Push the plugins and return to the Kernel
	$self->plugins(\@plugins);
	return 0;
}

sub exec {

}

1;
