package EventServer::API;

use strict;
use Moose;

has 'kernel' => (
	is => 'rw',
	isa => 'HashRef',
	default => {{}}
);

sub kernel {
	my $self = shift;
	return $self->kernel->{KERNEL};
}

sub heap {
	my $self = shift;
	return $self->kernel->{HEAP};
}

sub recv {
	my $self = shift;
	return $self->kernel->{RECV};
}
